package Inheritance;

public class Car extends Vehicle{
    public static void main(String[] args) {
        Car c = new Car();
        c.name = "BMW";
        System.out.println("Vehicle name: " + c.name);
        c.startVehicle();
        c.applyBrakes();
        c.stopVehicle();
    }
}