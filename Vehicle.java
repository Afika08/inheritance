package Inheritance;

public class Vehicle {
    String name;

    public void startVehicle() {
        System.out.println("Starting the vehicle");
    }

    public void applyBrakes() {
        System.out.println("Apply brakes");
    }

    public void stopVehicle() {
        System.out.println("Stopping the vehicle");
    }

}