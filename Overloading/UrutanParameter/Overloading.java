package Inheritance.Overloading.UrutanParameter;

public class Overloading {

    //OVERLOADING DENGAN urutan PARAMETER

    public void penjumlahan(int x, double y){
        System.out.println(x + y);
    }

    public void penjumlahan(double y, int x){
        System.out.println(x + y);
    }

    public static void main(String[] args) {
        Overloading tambah= new Overloading();
        tambah.penjumlahan(7,6.4);
        tambah.penjumlahan(3.3,4);
    }
}

