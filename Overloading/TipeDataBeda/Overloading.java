package Inheritance.Overloading.TipeDataBeda;

public class Overloading {

    //OVERLOADING DENGAN beda tipe data

    public void penjumlahan(int x, long y){
        System.out.println(x + y);
    }

    public void penjumlahan(long x, int y, double z){
        System.out.println(x + y + z);
    }

    public static void main(String[] args) {
        Overloading tambah= new Overloading();
        tambah.penjumlahan(7, 5);
        tambah.penjumlahan(3, 4, 6);
    }

}


