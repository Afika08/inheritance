package Inheritance.Overloading.BedaParameter;

public class Overloading {
    //OVERLOADING DENGAN JUMLAH PARAMETER

    public void penjumlahan(int x, int y){
        System.out.println(x + y);
    }

    public void penjumlahan(int x, int y, int z){
        System.out.println(x + y + z);
    }

    public static void main(String[] args) {
        Overloading tambah= new Overloading();
        tambah.penjumlahan(7, 5);
        tambah.penjumlahan(3, 4, 6);
    }

}

