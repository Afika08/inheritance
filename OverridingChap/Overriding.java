package Inheritance.OverridingChap;

public class Overriding {
    public static void main(String[] args) {
        Toyota T= new Toyota();
        Mobil M= new Mobil();
        Mobil MT= new Toyota();

        T.harga();
        M.harga();
        MT.harga();
    }
}